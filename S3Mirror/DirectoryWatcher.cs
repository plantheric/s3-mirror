﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace S3Mirror
{
    class DirectoryWatcher: IDisposable
    {
        public DirectoryWatcher(string path, Action<string> upload)
        {
            Upload = upload;
            Watcher.Path = path;
            Watcher.NotifyFilter = NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;

            Watcher.Changed += new FileSystemEventHandler(OnChanged);
            Watcher.Created += new FileSystemEventHandler(OnChanged);
            Watcher.Deleted += new FileSystemEventHandler(OnChanged);
            Watcher.Renamed += new RenamedEventHandler(OnRenamed);
            Watcher.EnableRaisingEvents = true;
        }

        public void StopWatching()
        {
            Watcher.EnableRaisingEvents = false;
        }

        private void OnChanged(object source, FileSystemEventArgs e)
        {
            Upload(e.FullPath);
        }

        private void OnRenamed(object source, RenamedEventArgs e)
        {
            Upload(e.FullPath);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                Watcher.Dispose();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        
        private FileSystemWatcher Watcher = new FileSystemWatcher();
        private Action<string> Upload;
    }
}
