﻿using System;
using System.Collections.Generic;
using System.Configuration;

namespace S3Mirror
{
    class MirrorConfig : ConfigurationSection
    {
        public static MirrorConfig GetConfig()
        {
            return (MirrorConfig)ConfigurationManager.GetSection("MirrorConfig") ?? new MirrorConfig();
        }

        [ConfigurationProperty("Bucket", IsRequired = true)]
        public string Bucket
        {
            get { return this["Bucket"] as string; }
        }

        [ConfigurationProperty("Region", IsRequired = true)]
        public string Region
        {
            get { return this["Region"] as string; }
        }

        [ConfigurationProperty("AccessKey", IsRequired = true)]
        public string AccessKey
        {
            get { return this["AccessKey"] as string; }
        }

        [ConfigurationProperty("SecretKey", IsRequired = true)]
        public string SecretKey
        {
            get { return this["SecretKey"] as string; }
        }

        [ConfigurationProperty("KeyPrefix", IsRequired = true)]
        public string KeyPrefix
        {
            get { return this["KeyPrefix"] as string; }
        }

        [ConfigurationProperty("NoUpload", DefaultValue = false)]
        public bool NoUpload
        {
            get { return (bool)this["NoUpload"]; }
        }

        [ConfigurationProperty("MaxConcurrentUploads", DefaultValue = 2)]
        public int MaxConcurrentUploads
        {
            get { return (int)this["MaxConcurrentUploads"]; }
        }

        [ConfigurationProperty("Directories")]
        [ConfigurationCollection(typeof(ConfigCollection<DirectoryMirror>), AddItemName = "Directory")]
        public ConfigCollection<DirectoryMirror> Directories
        {
            get { return this["Directories"] as ConfigCollection<DirectoryMirror>; }
        }

        public string FullKeyPrefix(string subKeyPrefix)
        {
            return KeyPrefix + @"/" + subKeyPrefix + @"/";
        }
    }

    public class ConfigCollection<T> : ConfigurationElementCollection, IEnumerable<T> where T : DirectoryMirror, new() 
    {
        public T this[int index]
        {
            get { return base.BaseGet(index) as T; }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new T();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((T)element).KeyPrefix;
        }

        public new IEnumerator<T> GetEnumerator()
        {
            int count = base.Count;
            for (int i = 0; i < count; i++)
            {
                yield return this[i];
            }
        }
    }

    public class DirectoryMirror : ConfigurationElement
    {
        [ConfigurationProperty("Path", IsRequired = true)]
        public string Path
        {
            get { return this["Path"] as string; }
        }

        [ConfigurationProperty("KeyPrefix", IsRequired = true)]
        public string KeyPrefix
        {
            get { return this["KeyPrefix"] as string; }
        }
    }
}
