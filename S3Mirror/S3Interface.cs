﻿using System;
using System.Collections.Generic;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using log4net;


namespace S3Mirror
{
    class S3Interface
    {
        public static void Setup(string accessKey, string secretKey, string bucket, string region)
        {
            AWSAccessKey = accessKey;
            AWSSecretKey = secretKey;
            AWSBucket = bucket;
            AWSRegion = region;
        }

        public List<S3Object> ObjectsFromKey(string key)
        {
            List<S3Object> s3Files = new List<S3Object>();

            using (var S3Client = AWSClientFactory.CreateAmazonS3Client(AWSAccessKey, AWSSecretKey, RegionEndpoint.GetBySystemName(AWSRegion)))
            {
                try
                {
                    var request = new ListObjectsRequest { BucketName = AWSBucket, Prefix = key, Delimiter = @"/" };

                    do
                    {
                        var response = S3Client.ListObjects(request);
                        s3Files.AddRange(response.S3Objects);

                        if (response.IsTruncated)
                            request.Marker = response.NextMarker;
                        else
                            request = null;

                    } while (request != null);
                }
                catch (Exception e)
                {
                    log.Error("ObjectsFromKey", e);
                }
            }
            return s3Files;
        }

        public Dictionary<string, string> GetObjectMetadata(List<S3Object> objects)
        {
            var metadata = new Dictionary<string, string>();

            using (var S3Client = AWSClientFactory.CreateAmazonS3Client(AWSAccessKey, AWSSecretKey, RegionEndpoint.GetBySystemName(AWSRegion)))
            {
                foreach (var s3Object in objects)
                {
                    var request = new GetObjectMetadataRequest { BucketName = AWSBucket, Key = s3Object.Key };
                    var response = S3Client.GetObjectMetadata(request);
                    metadata = response.Metadata.ToDictionary();
                }
            }
            return metadata;
        }

        private static string AWSAccessKey;
        private static string AWSSecretKey;
        private static string AWSBucket;
        private static string AWSRegion;

        private static readonly ILog log = LogManager.GetLogger(typeof(S3Interface));
    }

    public static class S3Extensions
    {
        public static Dictionary<string, string> ToDictionary(this MetadataCollection collection)
        {
            var dictionary = new Dictionary<string, string>();
            foreach (string key in collection.Keys)
                dictionary.Add(key, collection[key]);

            return dictionary;
        }
    }
}
