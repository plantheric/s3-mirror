﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace S3Mirror
{
    class S3Mirror : IDisposable
    {
        public S3Mirror()
        {
            Config = MirrorConfig.GetConfig();
            UploaderLock = new Object();
            Uploader = new Upload(Config.AccessKey, Config.SecretKey, Config.Bucket, Config.Region, Config.KeyPrefix, Config.MaxConcurrentUploads);
        }

        public void Start()
        {
            Log.Debug("Start");

            S3Interface.Setup(Config.AccessKey, Config.SecretKey, Config.Bucket, Config.Region);

            var existsingFiles = Config.Directories.SelectMany(directory =>
                {
                    var files = new UnMirroredFiles(directory.Path, Config.FullKeyPrefix(directory.KeyPrefix));
                    return files.GetFiles().Select(f => new UploadItem { FilePath = f, KeyPrefix = directory.KeyPrefix });
                });

            lock (UploaderLock)
            {
                Uploader.TransferFiles(existsingFiles);
            }
            Watchers = Config.Directories.Select(directory => new DirectoryWatcher(directory.Path, f =>
                        {
                            lock (UploaderLock)
                                Uploader.TransferFiles(new UploadItem { FilePath = f, KeyPrefix = directory.KeyPrefix });
                        })).ToList();
        }

        public void Stop()
        {
            Log.Info("Stop uploader");

            Watchers.ForEach(w => w.StopWatching());
            lock (UploaderLock)
                Uploader.WaitForUploads();
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Uploader.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private Upload Uploader;
        private Object UploaderLock;
        private MirrorConfig Config;
        private List<DirectoryWatcher> Watchers;

        private static readonly ILog Log = LogManager.GetLogger(typeof(S3Mirror));
    }
}
