﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using log4net;

namespace S3Mirror
{
    class UnMirroredFiles
    {
        public UnMirroredFiles(string localDirectory, string fullKeyPrefix)
        {
            LocalDirectory = localDirectory;
            FullKeyPrefix = fullKeyPrefix;
        }

        public List<string> GetFiles()
        {
            var uploads = new List<string>();
            try
            {
                Log.DebugFormat("Start GetFilesToUpload for {0}", LocalDirectory);

                var s3Objects = new S3Interface().ObjectsFromKey(FullKeyPrefix);
                var localPaths = Directory.GetFiles(LocalDirectory);

                //  Make Dictionaries with the file name and mod date for the local and remote lists
                var remotes = s3Objects.ToDictionary(o => o.Key.Substring(FullKeyPrefix.Length), o => o.LastModified);
                var locals = localPaths.ToDictionary(p => Path.GetFileName(p), p => File.GetLastWriteTimeUtc(p));

                locals.ForEach(f => Log.DebugFormat("Local , {0} : {1}", f.Key, f.Value));
                remotes.ForEach(f => Log.DebugFormat("Remote, {0} : {1}", f.Key, f.Value));

                //  Make list of file names that only local or newer on local
                var newNames = locals.Where(l =>                                                        //  Upload file when:-
                                            (!remotes.ContainsKey(l.Key)) ||                            //  File not on remote
                                            (remotes.ContainsKey(l.Key) && l.Value > remotes[l.Key])).  //  File on remote but newer version on local

                                            Select(p => p.Key).ToList();                                //  convert to list of file names

                uploads = newNames.ConvertAll(fn => Path.Combine(LocalDirectory, fn));
            }
            catch (Exception e)
            {
                Log.Error("GetFilesToUpload error", e);
            }

            Log.DebugFormat("End GetFilesToUpload found {0}", uploads.Count);

            return uploads;
        }

        private string LocalDirectory;
        private string FullKeyPrefix;

        private static readonly ILog Log = LogManager.GetLogger(typeof(UnMirroredFiles));
    }
}
