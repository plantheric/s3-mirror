﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;
using log4net;

namespace S3Mirror
{
    class UploadItem
    {
        public string FilePath;
        public string KeyPrefix;
    }

    class Upload : IDisposable
    {
        public Upload(string accessKey, string secretKey, string bucket, string region, string keyPrefix, int maxConcurrentUploads)
        {
            AWSBucket = bucket;
            KeyPrefix = keyPrefix;
            MaxConcurrentUploads = maxConcurrentUploads;
            S3Client = new Lazy<IAmazonS3>(() => AWSClientFactory.CreateAmazonS3Client(accessKey, secretKey, RegionEndpoint.GetBySystemName(region)));
            TransferUtility = new Lazy<TransferUtility>(() => new TransferUtility(S3Client.Value));
            Thread = new Thread(Run);
            Thread.Start();
        }

        public void TransferFiles(UploadItem upload)
        {
            if (!Cancel.IsCancellationRequested)
                PendingUploads.Add(upload);
        }

        public void TransferFiles(IEnumerable<UploadItem> uploads)
        {
            foreach (var upload in uploads)
                TransferFiles(upload);
        }

        public void WaitForUploads()
        {
            Cancel.Cancel();
            FlushCompleted();

            lock (TaskLock)
            {
                Task.WaitAll(Tasks.ToArray());
            }
        }

        private void Run()
        {
            while (!Cancel.IsCancellationRequested)
            {
                FlushCompleted();
                try
                {
                    if (Tasks.Count < MaxConcurrentUploads)
                    {
                        var upload = PendingUploads.Take(Cancel.Token);
                        PerformUpload(upload);
                    }
                    else
                    {
                        Log.Debug("Waiting...");
                        Task.WaitAny(Tasks.ToArray(), Cancel.Token);
                    }
                }
                catch (Exception e)
                {
                    if (!(e is System.OperationCanceledException))
                        Log.Error("TransferFiles error", e);
                }
            }
            Log.Debug("End TransferFiles");
        }

        private void PerformUpload(UploadItem upload)
        {
            string fileKey = FileKey(upload);
            var request = new TransferUtilityUploadRequest { BucketName = AWSBucket, Key = fileKey };

            request.FilePath = upload.FilePath;
            var task = TransferUtility.Value.UploadAsync(request);
            task.ContinueWith((prev) => FlushCompleted());
            lock (TaskLock)
                Tasks.Add(task);

            Log.InfoFormat("Scheduled upload '{0}' to '{1}'", upload.FilePath, fileKey);
        }

        private void FlushCompleted()
        {
            lock (TaskLock)
                Tasks.RemoveAll(t => t.IsCompleted);
        }

        private string FileKey(UploadItem item)
        {
            return KeyPrefix + @"/" + item.KeyPrefix + @"/" + Path.GetFileName(item.FilePath);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                Cancel.Dispose();
                PendingUploads.Dispose();
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private string AWSBucket;
        private string KeyPrefix;

        private Lazy<IAmazonS3> S3Client;
        private Lazy<TransferUtility> TransferUtility;

        private List<Task> Tasks = new List<Task>();
        private Object TaskLock = new Object();
        private int MaxConcurrentUploads;

        private Thread Thread;
        private CancellationTokenSource Cancel = new CancellationTokenSource();
        private BlockingCollection<UploadItem> PendingUploads = new BlockingCollection<UploadItem>();

        private static readonly ILog Log = LogManager.GetLogger(typeof(Upload));
    }
}
