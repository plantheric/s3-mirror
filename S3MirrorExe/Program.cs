﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace S3Mirror
{
    class Program
    {
        static void Main(string[] args)
        {
            XmlConfigurator.Configure();

            var mirror = new S3Mirror();
            mirror.Start();

            while (Console.Read() != 'q') ;

            mirror.Stop();
            Log.Info("End");

            while (Console.Read() != 'q') ;
        }

        private static readonly ILog Log = LogManager.GetLogger(typeof(Program));
    }
}
