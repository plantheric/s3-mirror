﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace S3Mirror
{
    class S3MirrorService : System.ServiceProcess.ServiceBase
    {
        public S3MirrorService()
        {
            this.ServiceName = "S3Mirror";
            this.CanStop = true;
            this.CanPauseAndContinue = true;
            this.AutoLog = true;

            XmlConfigurator.Configure();
            Mirror = new S3Mirror();
        }

        public static void Main()
        {
            System.ServiceProcess.ServiceBase.Run(new S3MirrorService());
        }

        protected override void OnStart(string[] args)
        {
            Mirror.Start();
        }

        protected override void OnStop()
        {
            base.OnStop();
            Mirror.Stop();
        }

        private S3Mirror Mirror;
    }
}
