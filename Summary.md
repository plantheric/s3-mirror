[S3 Mirror](https://bitbucket.org/plantheric/s3-mirror) is a tool for Windows that allows you to mirror local directories to Amazon S3.  
S3 Mirror can be run as either a command line application or installed as a Windows service.
